//
//  AppDelegate.h
//  Rest_App
//
//  Created by Bruno Tavares on 03/02/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

